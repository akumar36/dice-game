import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DiceService {
private _randomDiceData = new Subject<any>();
_diceResultData$ =this._randomDiceData.asObservable();
  constructor() { }

  sendRandomData(save:any){
    this._randomDiceData.next(save);
   // console.log(rand);
  }

}
