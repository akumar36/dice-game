import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DiceService } from '../dice.service';
@Component({
  selector: 'app-dice',
  templateUrl: './dice.component.html',
  styleUrls: ['./dice.component.css']
})
export class DiceComponent implements OnInit {

  playersForm: FormGroup;

  dummy = [];
  players = [2, 3, 4, 5];

  dice = ['A', 'K', 'Q', 'J', '10', '9'];
  submitGame = {};
  //rand: any = {};
  count: number = 0;
  disabled: any;
  save: any = {};
  final: number=2;
  constructor(private _DiceService: DiceService) { }


  ngOnInit() {
    this.playersForm = new FormGroup({
      players: new FormControl()

    });
  }

  totalPlayers(): void {
    this.dummy = [];
    this.final = this.playersForm.get('players').value;
    for (let i = 1; i <= this.final; i++) {
      let temp = i;
      this.dummy.push(temp);
    }
  }

  diceRoll(id: number) {
    this.count += 1;

    let collections = [];
    for (var j = 0; j <= 4; j++) {
      collections[j] = this.dice[Math.floor(Math.random() * this.dice.length)];
    }

    this.save["player_" + id] = (collections);
    this._DiceService.sendRandomData(this.save);
    // this.save.push({ player: id, dice_result: collections })
    // this._DiceService.sendRandomData(this.save);

    // if (this.save.length === 3) {
    //   this.disabled = true;
    // }



  }

  trackByFn(i: number, v: any): string {
    return v.numBox;
  }
  resetForm() {
    this.save = [];
    this._DiceService.sendRandomData(this.save);
    this.dummy = [];
    this.disabled = false;
    this.count = 0;
   // this.rand = [];
    console.clear();
  }
}

